package com.app.school.management.system.app.schoolapp;
import com.app.school.management.system.app.schoolapp.model.ApiSchool;
import com.app.school.management.system.app.schoolapp.model.SAT;

import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Response;

import static junit.framework.TestCase.assertTrue;

public class GetScoresTest extends BaseApiReference {
    @Test
    public void getScoresTest() {
        // Make Retrofit API object
        ApiSchool api = getApiSchool();
        Call<ArrayList<SAT>> call = api.getSchoolScore();
        try {
            Response<ArrayList<SAT>> response = call.execute();
            ArrayList<SAT> schoolScores = response.body();
            if( schoolScores == null || schoolScores.size() == 0 )
                return;
            SAT score = schoolScores.get(0);
            assertTrue(response.isSuccessful());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
