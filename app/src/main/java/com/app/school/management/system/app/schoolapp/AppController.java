package com.app.school.management.system.app.schoolapp;

import android.app.Application;
import android.content.Context;

public class AppController extends Application {

    private static Context mAppContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mAppContext = this;
        this.setAppContext(getApplicationContext());
    }

    public static Context getAppContext() {
        return mAppContext;
    }

    public void setAppContext(Context mAppContext) {
        this.mAppContext = mAppContext;
    }
}
