package com.app.school.management.system.app.schoolapp.model;

import com.app.school.management.system.app.schoolapp.util.Constants;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

public interface ApiSchool {
    public static final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(Constants.baseURl)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    @GET("s3k6-pzi2.json")
    Call<ArrayList<School>> getSchoolList();
    @GET("f9bf-2cp4.json")
    Call<ArrayList<SAT>> getSchoolScore();

}
