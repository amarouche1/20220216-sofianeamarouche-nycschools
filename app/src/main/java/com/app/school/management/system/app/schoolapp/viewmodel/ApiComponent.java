package com.app.school.management.system.app.schoolapp.viewmodel;


import com.app.school.management.system.app.schoolapp.model.ApiSchool;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApiModule.class})
interface ApiComponent {
    ApiSchool provideApi();
    void inject(DataViewModel viewModel);
}
