package com.app.school.management.system.app.schoolapp.viewmodel;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.school.management.system.app.schoolapp.db.SchoolDatabase;
import com.app.school.management.system.app.schoolapp.model.SAT;
import com.app.school.management.system.app.schoolapp.util.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.room.Room;

public class DetailViewModel extends ViewModel {

    private String DB_NAME = "db_school";

    private SchoolDatabase noteDatabase;
    private Context context;
    private RequestQueue requestQueue;
    public DetailViewModel(Context context) {
        noteDatabase = Room.databaseBuilder(context, SchoolDatabase.class, DB_NAME).build();
        this.context=context;
        requestQueue = Volley.newRequestQueue(context);
    }

    public void insertSATData(String name, String reading, String math, String writing) {
        SAT sat = new SAT();
        sat.setName(name);
        sat.setReading(reading);
        sat.setMath(math);
        sat.setWriting(writing);

        insertSAT(sat);
    }

    // This function will get the data of SAT average scores and store in the  room database
    public void getSATData() {
        Log.d("getSat", "functions");
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.baseURl+"f9bf-2cp4.json",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("getSat", response);
                        try {
                            JSONArray schoolsJsonArray=new JSONArray(response);
                            for (int i = 0; i < schoolsJsonArray.length(); i++) {
                                JSONObject satData = schoolsJsonArray.getJSONObject(i);
                                String name=satData.getString("school_name");
                                String reading= satData.getString("sat_critical_reading_avg_score");
                                String math= satData.getString("sat_math_avg_score");
                                String writing= satData.getString("sat_writing_avg_score");
                                insertSATData(name,reading,math,writing);
                                Log.d("getSat", name.toLowerCase());

                            }
                        } catch (JSONException e) {
                            Log.d("parsingError",e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("parsingError",error.toString()+"vollery Error");
            }
        });

        requestQueue.add(stringRequest);

    }

    public void insertSAT(final SAT sat) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                Long aLong=noteDatabase.daoAccess().insertSATToDB(sat);
                Log.d("inserTaskcheck",aLong+"ok");
                return null;
            }
        }.execute();
    }

    public LiveData<SAT> getSatData(String name) {
        return noteDatabase.daoAccess().getSAT(name);
    }

    public LiveData<List<SAT>> getCount() {
        return noteDatabase.daoAccess().fetchAllSatITems();
    }



}
