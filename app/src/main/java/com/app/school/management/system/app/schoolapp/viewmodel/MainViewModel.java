package com.app.school.management.system.app.schoolapp.viewmodel;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.school.management.system.app.schoolapp.db.SchoolDatabase;
import com.app.school.management.system.app.schoolapp.model.School;
import com.app.school.management.system.app.schoolapp.util.Constants;
import com.app.school.management.system.app.schoolapp.view.ui.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.room.Room;

public class MainViewModel extends ViewModel {

    private String DB_NAME = "db_school";
    private Context context;
    private RequestQueue requestQueue;

    private SchoolDatabase noteDatabase;
    public MainViewModel(Context context) {
        noteDatabase = Room.databaseBuilder(context, SchoolDatabase.class, DB_NAME).build();
        this.context=context;
        requestQueue = Volley.newRequestQueue(context);
    }
    public void insertSchool(String name, String address, String overview, String phone, String latitude, String longitude, String website) {
        School school = new School();
        school.setName(name);
        school.setLocation(address);
        school.setOverview(overview);
        school.setPhone(phone);
        school.setLatitude(latitude);
        school.setLongitude(longitude);
        school.setWebsite(website);

        insertTask(school);
    }

    public void getSchoolsData() {
//        binding.progressBar.setVisibility(View.VISIBLE);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.baseURl+"s3k6-pzi2.json",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("errorInResponse", response);
//                        binding.progressBar.setVisibility(View.GONE);
                        try {
                            //getting JsonArray
                            JSONArray schoolsJsonArray=new JSONArray(response);
                            for (int i = 0; i < schoolsJsonArray.length(); i++) {
                                //getting and parsing jSon Objects
                                JSONObject schoolObj = schoolsJsonArray.getJSONObject(i);
                                String name=schoolObj.getString("school_name");
                                String address= schoolObj.getString("location");
                                String overview= schoolObj.getString("overview_paragraph");
                                String phone= schoolObj.getString("phone_number");
                                String latitude= schoolObj.getString("latitude");
                                String longitude= schoolObj.getString("longitude");
                                String website= schoolObj.getString("website");
                                //inserting the data into room School Entity
                                insertSchool(name,address,overview,phone,latitude,longitude,website);
                            }
                            //this function will display the data on the view

                        } catch (JSONException e) {
//                            binding.progressBar.setVisibility(View.GONE);
                            Log.d("parsingError",e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        requestQueue.add(stringRequest);

    }


    public void insertTask(final School school) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                noteDatabase.daoAccess().insertSchool(school);
                return null;
            }
        }.execute();
    }

    public void deleteTask(final School school) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                noteDatabase.daoAccess().deleteTask(school);
                return null;
            }
        }.execute();
    }

    public void updateTask(final School school) {
//        school.setModifiedAt(AppUtils.getCurrentDateTime());

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                noteDatabase.daoAccess().updateTask(school);
                return null;
            }
        }.execute();
    }

    public LiveData<List<School>> getTasks() {
        return noteDatabase.daoAccess().fetchAllSchools();
    }
}
