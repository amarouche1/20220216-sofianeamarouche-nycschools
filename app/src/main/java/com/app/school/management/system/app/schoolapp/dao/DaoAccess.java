package com.app.school.management.system.app.schoolapp.dao;

import com.app.school.management.system.app.schoolapp.model.SAT;
import com.app.school.management.system.app.schoolapp.model.School;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public interface DaoAccess {

    @Insert
    Long insertSchool(School school);

    @Query("SELECT * FROM School")
    LiveData<List<School>> fetchAllSchools();

    @Insert
    Long insertSATToDB(SAT sat);


    @Query("SELECT * FROM SAT WHERE name =:school")
    LiveData<SAT> getSAT(String school);

    @Query("SELECT * FROM SAT")
    LiveData<List<SAT>> fetchAllSatITems();


    @Update
    void updateTask(School school);


    @Delete
    void deleteTask(School school);
}
