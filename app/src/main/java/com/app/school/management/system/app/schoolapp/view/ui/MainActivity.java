package com.app.school.management.system.app.schoolapp.view.ui;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;


import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.school.management.system.app.schoolapp.R;
import com.app.school.management.system.app.schoolapp.model.SAT;
import com.app.school.management.system.app.schoolapp.model.School;
import com.app.school.management.system.app.schoolapp.viewmodel.DetailViewModel;
import com.app.school.management.system.app.schoolapp.util.Constants;
import com.app.school.management.system.app.schoolapp.view.adapter.SchoolAdapter;
import com.app.school.management.system.app.schoolapp.databinding.ActivityMainBinding;
import com.app.school.management.system.app.schoolapp.viewmodel.MainViewModel;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding binding;
    public List<School> schoolDataArrayList;
    public static SchoolAdapter adapter;
    private RequestQueue requestQueue;

    private DetailViewModel dataViewModel;
    private MainViewModel mainViewModel;

    boolean doubleBackToExitPressedOnce = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this , R.layout.activity_main);
        initView();

        updateSchoolsList();

        updateSATLIst();

		searchSchool();
    }

    private void updateSATLIst() {
        dataViewModel.getCount().observe(this, new Observer<List<SAT>>() {
            @Override
            public void onChanged(@Nullable List<SAT> list) {
                if(list.size() > 0) {
                    Log.d("isData",list.size()+"");
                } else{
                    Log.d("isData","NO");
                    dataViewModel.getSATData();
                }
            }
        });
    }


    //initialize the array and database repository reference
    private void initView() {
        schoolDataArrayList = new ArrayList<>();
        requestQueue = Volley.newRequestQueue(MainActivity.this);
        dataViewModel = new DetailViewModel(MainActivity.this);
        mainViewModel = new MainViewModel(getApplicationContext());
    }
    // this function will chek if data is available in the local database then load the data form local database otherwise load data form server and save it in database
    private void updateSchoolsList() {
        binding.progressBar.setVisibility(View.VISIBLE);
        mainViewModel.getTasks().observe(this, new Observer<List<School>>() {
            @Override
            public void onChanged(@Nullable List<School> schools) {
                // if data is available show it on the UI
                if(schools.size() > 0) {
                    Log.d("StringSize", schools.size()+"");
                    Log.d("isData","Yes");
                    binding.progressBar.setVisibility(View.GONE);
                    schoolDataArrayList=schools;
                    binding.noData.setVisibility(View.GONE);
                    binding.recyclerView.setVisibility(View.VISIBLE);
                    binding.recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));
                    adapter = new SchoolAdapter(schools,MainActivity.this);
                    binding.recyclerView.setAdapter(adapter);

                } else{
                    Log.d("isData","NO");
                    mainViewModel.getSchoolsData();
                }
            }
        });
    }

    private void updateEmptyView() {
        binding.noData.setVisibility(View.VISIBLE);
        binding.recyclerView.setVisibility(View.GONE);
    }

    //this function will help in searching a school by name
    private void searchSchool(){
        binding.inputSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().trim().length() == 0) {
                    if (schoolDataArrayList.size() != 0) {
                        binding.recyclerView.setVisibility(View.VISIBLE);
                        binding.noData.setVisibility(View.GONE);
                    } else {
                        binding.recyclerView.setVisibility(View.GONE);
                        binding.noData.setVisibility(View.VISIBLE);
                    }

                    adapter = new SchoolAdapter(schoolDataArrayList, MainActivity.this);
                    binding.recyclerView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                } else {
                    ArrayList<School> clone = new ArrayList<>();
                    //match the user entered keyword with all the schools available in the list and get matching records
                    for (School element : schoolDataArrayList) {
                        if (element.getName().toLowerCase().contains(s.toString().toLowerCase())) {
                            clone.add(element);
                        }
                    }
                    if (clone.size() != 0) {
                        binding.recyclerView.setVisibility(View.VISIBLE);
                        binding.noData.setVisibility(View.GONE);
                    } else {
                        binding.recyclerView.setVisibility(View.GONE);
                        binding.noData.setVisibility(View.VISIBLE);
                    }

                    adapter = new SchoolAdapter(clone, MainActivity.this);
                    binding.recyclerView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }


    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }
}