package com.app.school.management.system.app.schoolapp.viewmodel;

import com.app.school.management.system.app.schoolapp.model.ApiSchool;
import javax.inject.Inject;
import androidx.lifecycle.ViewModel;

public class DataViewModel extends ViewModel {
    @Inject
    ApiSchool mApi;
    // Constructor - Inject Retrofit API object
    public DataViewModel() {
        ApiComponent component = DaggerApiComponent.builder().build();
        component.inject(this);
    }
}
